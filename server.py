#!/usr/bin/python2
# -*- coding: utf-8 -*-

# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import sys

import pygame
import threading
import time

from Classes.Partie import Partie
from Classes.Player1 import Player1
from Classes.Player2 import Player2
from PodSixNet.Channel import Channel
from PodSixNet.Server import Server
from Utils import Utils

parties = {}  # Les parties en fonction de la room


class ClientChannel(Channel):
    player = None

    def __init__(self, *args, **kwargs):
        Channel.__init__(self, *args, **kwargs)
        self.room = None
        self.player = None

    def setNomRoom(self, name):
        self.room = name

    def Close(self):
        self._server.del_client(self)

    def Network(self, data):
        pass

    def Network_playerName(self, data):
        parties[self.room].getPlayer(self).player.setName(data["playerName"])
        if len(parties[self.room].getPlayers()) == 2:
            parties[self.room].getPlayer2(self).Send(
                {"action": "playerName", "playerName": parties[self.room].getPlayer(self).player.playerName})
            parties[self.room].getPlayer(self).Send(
                {"action": "playerName", "playerName": parties[self.room].getPlayer2(self).player.playerName})

    def Network_keys(self, data):
        touches = data["keystrokes"]
        player = parties[self.room].getPlayer(self)
        if data["idPlayer"] == player.player.idPlayer:
            if touches[pygame.K_UP]:
                player.player.up()
            if touches[pygame.K_DOWN]:
                player.player.down()
            if touches[pygame.K_LEFT]:
                player.player.left()
            if touches[pygame.K_RIGHT]:
                player.player.right()

            if touches[pygame.K_SPACE]:
                bomb = player.player.bomb()
                if bomb is not None:
                    parties[player.room].sprite_bomb.add(bomb)
                    x, y = Utils.get_case_from_position(bomb.rect.center)
                    parties[player.room].carte_save[y][x] = Utils.BOMB
            if touches[pygame.K_m]:
                print(" MAPS ")
                for line in range(len(parties[player.room].carte_save)):
                    print(str(parties[player.room].carte_save[line]))
                print(" END MAPS ")


class MyServer(Server):
    channelClass = ClientChannel

    def __init__(self, *args, **kwargs):
        Server.__init__(self, *args, **kwargs)

        self.clients = []
        print('Server launched')

    # place a player in a room
    # return false if the room is not full after the entrance of the new player
    # return true if full
    def selectRoom(self, chan):
        # check if a room is available
        existRoomToStart = ""
        for nomRoom, partie in parties.items():
            if (len(partie.getPlayers()) < 2):
                existRoomToStart = nomRoom

        # creation
        if existRoomToStart == "":
            newRoom = "room" + str(len(parties))
            chan.setNomRoom(newRoom)
            parties[newRoom] = Partie(Utils.carte1())
            chan.player = Player1((Utils.SIZE, Utils.SIZE))
            parties[newRoom].addPlayer(chan, chan.player)
            return False
        else:
            # set the player in the room
            chan.setNomRoom(existRoomToStart)
            print(type(parties[existRoomToStart].players[0].player))
            # check the type of player already in the room
            if isinstance(parties[existRoomToStart].players[0].player, Player2):
                chan.player = Player1((Utils.SIZE, Utils.SIZE))
            else:
                chan.player = Player2(((Utils.SIZE * (Utils.NUMBER_BLOCK - 2)), Utils.SIZE * (Utils.NUMBER_BLOCK - 2)))

            parties[existRoomToStart].addPlayer(chan, chan.player)
            return True

    def Connected(self, channel, addr):
        print('New connection from : ' + str(addr))
        self.clients.append(channel)
        if self.selectRoom(channel):
            self.startPartie(parties[channel.room])

        for nomRoom, partie in parties.items():
            print nomRoom + ": " + str(len(partie.players))

    def startPartie(self, partie):
        partie.start = True
        print "Début de partie"
        info = {"action": "wall"}

        info["indestructibleWall"] = partie.normalizeIndestructibleWalls()
        info["destructibleWall"] = partie.normalizeDestructibleWallFirstSending()

        for client in partie.players:
            client.Send(info)
            client.Send({"action": "idPlayer", "idPlayer": id(client.player)})

        partie.players[0].Send({"action": "playerName", "playerName": partie.players[1].player.playerName})
        partie.players[1].Send({"action": "playerName", "playerName": partie.players[0].player.playerName})

    def del_client(self, channel):
        print('client deconnecte')
        room = channel.room
        parties[channel.room].removePlayer(channel)
        self.clients.remove(channel)
        # if the room is empty, it is destroyed
        if len(parties[room].players) == 0:
            parties.pop(room)
        # the players have to restart all their sprites
        else:
            parties[room].players[0].Send({"action": "rAz"})
        for nomRoom, partie in parties.items():
            print nomRoom + ": " + str(len(partie.players))

    # send the info to all the client in a room
    def send_all(self, partie, data):
        for client in partie.getPlayers():
            client.Send(data)

    # send all the data needed by the client
    def send_everything(self, partie):
        self.send_players(partie)
        self.send_bombs(partie)
        self.send_explosions(partie)
        self.send_walls(partie)
        self.send_bonus(partie)
        self.send_looser(partie)
        # self.bigPaquet(partie)

    # test if the reduction of packet has an impact
    def bigPaquet(self, partie):
        info = {"action": "all"}
        info["player1"] = partie.normalizePlayers()
        info["bomb"] = partie.normalizeBombs()
        info["explosion"] = partie.normalizeExplosions()
        info["wallDestruct"] = partie.normalizeDestructibleWall()
        info["bonus"] = partie.normalizeBonus()
        self.send_all(partie, info)
        self.send_looser(partie)

    def send_players(self, partie):
        info = {"action": "players"}
        info["player1"] = partie.normalizePlayers()
        self.send_all(partie, info)

    def send_looser(self, partie):
        if partie.looser != None:
            info = {"action": "looser", "looser": partie.looser.idPlayer}
            partie.rAz()
            for client in partie.players:
                client.player.rAz()
            self.send_all(partie, info)
            thread = threading.Thread(target=self.wait_and_restart_partie, args=(partie,))
            thread.start()
            print "Le thread a commencé"

    def wait_and_restart_partie(self, partie):
        time.sleep(5)
        if (len(partie.players) == 2):
            partie.setPlayersToDefaultPosition()
            self.send_all(partie, {"action": "rAz"})
            self.startPartie(partie)
        print "FIN DU THREAD"

    def send_bombs(self, partie):
        info = {"action": "bombs"}
        info["bomb"] = partie.normalizeBombs()
        self.send_all(partie, info)

    def send_explosions(self, partie):
        info = {"action": "explosions"}
        info["explosion"] = partie.normalizeExplosions()
        self.send_all(partie, info)

    def send_walls(self, partie):
        info = {"action": "walls"}
        info["wallDestruct"] = partie.normalizeDestructibleWall()
        self.send_all(partie, info)
        for wallDestruct in partie.sprite_wallDestruct:
            if wallDestruct.life <= 0:
                pygame.sprite.Sprite.kill(wallDestruct)

    def send_bonus(self, partie):
        info = {"action": "bonus"}
        info["bonus"] = partie.normalizeBonus()
        self.send_all(partie, info)
        pass


def main_prog():
    my_server = MyServer(localaddr=(sys.argv[1], int(sys.argv[2])))
    pygame.init()
    screen = pygame.display.set_mode((200, 100))
    background_image, background_rect = Utils.load_png('images/small/dirt.png')
    clock = pygame.time.Clock()
    pygame.display.set_caption("Server")

    while True:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return  # closing the window exits the program
        my_server.Pump()

        for nomRoom, partie in parties.items():
            partie.update()
            my_server.send_everything(partie)


if __name__ == '__main__':
    main_prog()
    sys.exit(0)
