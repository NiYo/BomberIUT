# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import os

import pygame

SIZE = 48
NUMBER_BLOCK = 15
SCREEN_WIDTH = NUMBER_BLOCK * SIZE
SCREEN_HEIGHT = NUMBER_BLOCK * SIZE
WALL = 1
BLANK = 0
WALL_DESTROY = 2
BOMB = 3
PLAYER_SPAWN = 0
BONUS_NOMBRE = 9
BONUS_PUISSANCE = 8


# FUNCTIONS
def load_png(name):
    """Load image and return image object"""
    fullname = os.path.join('.', name)
    try:
        image = pygame.image.load(fullname)
        if image.get_alpha is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    return image, image.get_rect()


def carte1():
    carte = \
        [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
         [1, 0, 0, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1],
         [1, 0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
         [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1],
         [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
         [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1],
         [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
         [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1],
         [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
         [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1],
         [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
         [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1],
         [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 0, 1],
         [1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 0, 0, 1],
         [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]
    return carte


def get_case_from_position(position):
    x, y = position
    return int(x / SIZE), int(y / SIZE)


class Table:
    def __init__(self):
        pass

    """
        in A but not in B
    """

    @staticmethod
    def diff(a, b):
        b = set(b)
        return [aa for aa in a if aa not in b]

    """
        in A and B
    """

    @staticmethod
    def union(a, b):
        b = set(b)
        return [aa for aa in a if aa in b]
