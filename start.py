# coding=utf-8
# Script that ask the player's pseudo and start the program

# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import tkFont
from Tkinter import *
from os import system


def quitGame():
    sys.exit(0)


def startGame():
    global nameText
    if nameText.get() != "" and ipText.get() != "" and portText.get() != "":
        fen.quit()


fen = Tk()
fen.title("Bomberman")
fen.iconbitmap('images/ico.ico')
fen.protocol('WM_DELETE_WINDOW', quitGame)
helv36 = tkFont.Font(fen, family="font/VCR_OSD_MONO_1.001.ttf", size=20, weight="bold")

# background
BGMenu = PhotoImage(file="images/bg_start_menu.gif")
a = Label(fen, image=BGMenu)
a.grid(row=0, column=0, rowspan=20, columnspan=7)

# texte
lab = Label(fen, text='Votre pseudo:', font=("Helvetica", 30), fg="navy")
lab.grid(row=6, column=2, rowspan=1, columnspan=3)

# texte
lab = Label(fen, text='ip:', font=("Helvetica", 20), fg="navy")
lab.grid(row=15, column=1, rowspan=1, columnspan=1)

# texte
lab = Label(fen, text='port:', font=("Helvetica", 20), fg="navy")
lab.grid(row=16, column=1, rowspan=1, columnspan=1)

# variable that will store the pseudo
nameText = StringVar()  # definition d'une variable-chaine pour recevoir la saisie d'un texte
nameText.set("Roger")  # facultatif: assigne une valeur à la variable

ipText = StringVar()
ipText.set("127.0.0.1")

portText = StringVar()
portText.set("8080")

# pseudo field
nameField = Entry(textvariable=nameText, width=30)
nameField.grid(row=7, column=2, rowspan=1, columnspan=3, ipady=5)

# ip field
ipField = Entry(textvariable=ipText, width=30)
ipField.grid(row=15, column=2, rowspan=1, columnspan=1, ipady=5)

# port field
portField = Entry(textvariable=portText, width=30)
portField.grid(row=16, column=2, rowspan=1, columnspan=1, ipady=5)

# JOUER button
okButton = Button(fen, text="Jouer", command=startGame, font=helv36)
quitButton = Button(fen, text="Quitter", command=quitGame, font=helv36)

# QUITTER button
quitButton.grid(row=8, column=3, rowspan=1, columnspan=3, ipady=8)
okButton.grid(row=8, column=1, rowspan=1, columnspan=3, ipady=8)

fen.mainloop()
try:
    fen.destroy()
except:
    print "Application deja fermee"

system("client.py " + nameText.get() + " " + ipText.get() + " " + portText.get())
