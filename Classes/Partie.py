# coding=utf-8
# -------------------
# -------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------

"""
Principe:
    un dictionnaire de partie contient toutes les parties
    La partie contient tous les sprites ainsi que les joueurs
    Elle possede sa carte
    Ses murs ...
    La partie posséde un parametre permettant de definir si elle a demarré
"""
import random

import pygame

from Classes.Bonus import Bonus, POWER, NUMBER
from Classes.Explosion import Explosion
from Classes.Wall import Wall
from Classes.WallDestruct import WallDestruct
from Utils import Utils


class Partie:
    def __init__(self, carte):
        self.carteForRestart = self.copyCarte(carte)
        self.carte_save = None
        self.players = []
        self.start = False
        self.sprite_wall = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
        self.sprite_wallDestruct = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
        self.sprite_player = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
        self.sprite_bomb = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
        self.sprite_explosion = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
        self.sprite_bonus = pygame.sprite.RenderClear()  # creation d'un groupe de
        self.looser = None
        self.init_map(carte)

    def copyCarte(self, carte):
        newList = []
        for ligne in carte:
            tmp = []
            for colonne in ligne:
                tmp.append(colonne)
            newList.append(tmp)
        return newList

    def start(self):
        self.start = True

    def update(self):
        self.sprite_wall.update()
        self.sprite_wallDestruct.update()
        self.sprite_bomb.update()
        self.sprite_bonus.update()
        for bomb in self.sprite_bomb:
            if bomb.timer == 0:  # explosion
                self.generate_explosions(bomb)
        for player in self.sprite_player:
            self.control_player_collision(player)
        self.sprite_player.update()
        self.sprite_explosion.update()

    def getPlayers(self):
        return self.players

    def getPlayer(self, channel):
        return self.players[self.players.index(channel)]

    def getPlayer2(self, channel):
        return self.players[(self.players.index(channel) + 1) % 2]

    def addPlayer(self, channel, sprite):
        self.players.append(channel)
        self.sprite_player.add(sprite)

    def removePlayer(self, channel):
        self.players.remove(channel)
        pygame.sprite.Sprite.kill(channel.player)
        self.rAz()

    def rAz(self):
        self.start = False
        self.looser = None
        # self.sprite_player.empty()
        self.sprite_wall.empty()  # creation d'un groupe de sprite
        self.sprite_wallDestruct.empty()  # creation d'un groupe de sprite
        self.sprite_bomb.empty()  # creation d'un groupe de sprite
        self.sprite_explosion.empty()  # creation d'un groupe de sprite
        self.sprite_bonus.empty()  # creation d'un groupe de
        self.init_map(self.copyCarte(self.carteForRestart))

    def setPlayersToDefaultPosition(self):
        for player in self.players:
            player.player.setInitialPosition()

    def normalizePlayers(self):
        tmp = []
        for client in self.sprite_player:
            tmp.append({"pos": client.rect.topleft, "id": client.idPlayer})
        return tmp

    def normalizeBombs(self):
        tmp = []
        for bomb in self.sprite_bomb:
            tmp.append({"pos": bomb.rect.topleft, "id": bomb.idBomb})
        return tmp

    def normalizeExplosions(self):
        tmp = []
        for explosion in self.sprite_explosion:
            if explosion.timer <= 1:
                tmp.append({"pos": explosion.rect.topleft, "id": explosion.idExplosion, "timer": explosion.timer})
        return tmp

    def normalizeIndestructibleWalls(self):
        tmp = []
        for wall in self.sprite_wall:
            tmp.append({"pos": wall.rect.topleft, "id": wall.idWall})
        return tmp

    def normalizeDestructibleWallFirstSending(self):
        tmp = []
        for wallDestruct in self.sprite_wallDestruct:
            tmp.append({"pos": wallDestruct.rect.topleft, "id": wallDestruct.idWall})
        return tmp

    def normalizeDestructibleWall(self):
        tmp = []
        for wallDestruct in self.sprite_wallDestruct:
            if wallDestruct.life <= 0:
                tmp.append({"pos": wallDestruct.rect.topleft, "id": wallDestruct.idWall})
        return tmp

    def normalizeBonus(self):
        tmp = []
        for bonus in self.sprite_bonus:
            tmp.append({"pos": bonus.rect.topleft, "id": bonus.idBonus, "type": bonus.type})
        return tmp

    def init_map(self, carte):
        self.carte_save = self.copyCarte(self.carteForRestart)
        for line in range(len(carte)):
            for cube in range(len(carte[line])):
                pos = (line * Utils.SIZE, cube * Utils.SIZE)
                if carte[line][cube] == Utils.WALL:
                    added_object = Wall(pos)
                    self.sprite_wall.add(added_object)
                elif carte[line][cube] == Utils.WALL_DESTROY:
                    added_object = WallDestruct(pos)
                    self.sprite_wallDestruct.add(added_object)
                elif carte[line][cube] == Utils.BLANK:
                    pass
                else:
                    pass

    def control_player_collision(self, player):
        bonus = pygame.sprite.spritecollide(player, self.sprite_bonus, False)
        for bonu in bonus:
            if bonu.type == NUMBER:
                player.number_bomb_max += 1
            if bonu.type == POWER:
                player.range += 1
            x, y = Utils.get_case_from_position(bonu.rect.center)
            self.carte_save[y][x] = Utils.BLANK
            pygame.sprite.Sprite.kill(bonu)
        looser = pygame.sprite.groupcollide(self.sprite_explosion, self.sprite_player, False, False)
        if len(looser) > 0:
            self.looser = (looser.values()[0])[0]
        x, y = Utils.get_case_from_position(player.rect.center)
        i = x - 1
        while (self.carte_save[y][i] not in [Utils.BOMB, Utils.WALL, Utils.WALL_DESTROY]) and i >= 0:
            i -= 1
        player.block_left = i * Utils.SIZE + Utils.SIZE
        # print("     left "+str(player.block_left)+":"+str(i)+":"+str(carte_save[y][i]))
        i = x + 1
        while (self.carte_save[y][i] not in [Utils.BOMB, Utils.WALL, Utils.WALL_DESTROY]) and i >= 0:
            i += 1
        player.block_right = i * Utils.SIZE
        # print("     right "+str(player.block_right)+":"+str(i)+":"+str(carte_save[y][i]))
        i = y - 1
        while (self.carte_save[i][x] not in [Utils.BOMB, Utils.WALL, Utils.WALL_DESTROY]) and i >= 0:
            i -= 1
        player.block_top = i * Utils.SIZE + Utils.SIZE
        # print("     top "+str(player.block_top)+":"+str(i)+":"+str(carte_save[i][x]))
        i = y + 1
        while (self.carte_save[i][x] not in [Utils.BOMB, Utils.WALL, Utils.WALL_DESTROY]) and i >= 0:
            i += 1
        player.block_bottom = i * Utils.SIZE
        # print("     bottom "+str(player.block_bottom)+":"+str(i)+":"+str(carte_save[i][x]))

    def generate_line_explosion(self, bomb, start, ranges, isHorizontal):
        for i in ranges:
            if isHorizontal:
                explosion = Explosion((i * Utils.SIZE, start * Utils.SIZE))
            else:
                explosion = Explosion((start * Utils.SIZE, i * Utils.SIZE))
            wallTouched = pygame.sprite.spritecollide(explosion, self.sprite_wall, False)
            wallDestructTouched = pygame.sprite.spritecollide(explosion, self.sprite_wallDestruct, False)
            bomb_touched = pygame.sprite.spritecollide(explosion, self.sprite_bomb, False)
            for bomb_touch in bomb_touched:
                if bomb_touch != bomb:
                    self.generate_explosions(bomb_touch)
            if len(wallTouched) > 0:
                break
            self.sprite_explosion.add(explosion)
            if len(wallDestructTouched) > 0:
                self.remove_life_wall(wallDestructTouched)
                break

    def generate_explosions(self, bomb):
        if bomb.timer != -1:
            bomb.timer = -1
            x, y = Utils.get_case_from_position(bomb.rect.center)
            # left
            self.generate_line_explosion(bomb, y, range(x - 1, x - 1 - bomb.range, -1), True)
            # bottom
            self.generate_line_explosion(bomb, x, range(y + 1, y + 1 + bomb.range), False)
            # right
            self.generate_line_explosion(bomb, y, range(x + 1, x + 1 + bomb.range), True)
            # top
            self.generate_line_explosion(bomb, x, range(y - 1, y - 1 - bomb.range, -1), False)
            # origine explosion
            explosion_origine = Explosion((x * Utils.SIZE, y * Utils.SIZE))
            self.sprite_explosion.add(explosion_origine)  # Explosion on the bomb
            wallDestructTouched = pygame.sprite.spritecollide(explosion_origine, self.sprite_wallDestruct, False)
            if len(wallDestructTouched) > 0:
                self.remove_life_wall(wallDestructTouched)
            self.carte_save[y][x] = Utils.BLANK
            pygame.sprite.Sprite.kill(bomb)

    # remove a life to a wall to destroy it and create a bonus randomly
    def remove_life_wall(self, wallsTouched):
        for wallDestruct in wallsTouched:
            wallDestruct.life -= 1
            if wallDestruct.life <= 0:
                x, y = Utils.get_case_from_position(wallDestruct.rect.center)
                self.carte_save[y][x] = Utils.BLANK
                if random.randint(0, 19) < 2:
                    if not random.getrandbits(1):
                        bonus = Bonus((x * Utils.SIZE, y * Utils.SIZE), POWER)
                        self.carte_save[y][x] = Utils.BONUS_PUISSANCE
                    else:
                        bonus = Bonus((x * Utils.SIZE, y * Utils.SIZE), NUMBER)
                        self.carte_save[y][x] = Utils.BONUS_NOMBRE
                    self.sprite_bonus.add(bonus)
