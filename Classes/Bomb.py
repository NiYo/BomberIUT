# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------

import pygame

from PodSixNet.Connection import ConnectionListener
from Utils import Utils
from Utils.Utils import load_png


class Bomb(pygame.sprite.Sprite):
    idBomb = None
    player = None
    timer = 120
    range = 1

    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = load_png('images/small/bombe1.png')
        self.idBomb = id(self)
        self.rect.topleft = pos

    def update(self):
        self.timer -= 1

    def set_position_from_player(self):
        x, y = self.player.rect.center
        x = int(round(x / Utils.SIZE, 0))
        y = int(round(y / Utils.SIZE, 0))
        self.rect.topleft = (x * Utils.SIZE, y * Utils.SIZE)

    def set_timer_from_player(self):
        self.timer = self.player.timer_bomb

    def set_range_from_player(self):
        self.range = self.player.range

    def set_params_from_player(self):
        self.set_position_from_player()
        self.set_range_from_player()
        self.set_timer_from_player()


class BombClient(Bomb, ConnectionListener):
    def __init__(self, pos, idBomb):
        Bomb.__init__(self, pos)
        self.idBomb = idBomb

    def set_position(self, position):
        self.rect.center = position

    def update(self):
        self.Pump()

    def Network_bomb_exploded(self, data):
        pass
