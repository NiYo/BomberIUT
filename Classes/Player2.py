# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import random

from Player1 import Player1
from PodSixNet.Connection import ConnectionListener
from Utils.Utils import load_png


class Player2(Player1):
    """ class of  """

    def __init__(self, pos):
        Player1.__init__(self, pos)
        if not random.getrandbits(1):
            self.image, self.rect = load_png('images/small/player2.png')
        else:
            self.image, self.rect = load_png('images/small/player1.png')
        self.rect.topleft = pos


class Player2Client(Player2, ConnectionListener):
    def __init__(self, pos, idPlayer):
        Player2.__init__(self, pos)
        self.idPlayer = idPlayer
        self.rect.topleft = pos

    def set_position(self, position):
        self.rect.center = position

    def update(self):
        self.Pump()
