# coding=utf-8

# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import random

from Bomb import *
from PodSixNet.Connection import ConnectionListener
from Utils.Utils import *


class Player1(pygame.sprite.Sprite):
    """ class of  """

    idPlayer = None
    speedMultiply = 1
    range = 2  # Range of the explosion
    timer_bomb = 120
    number_bomb_max = 1
    canBomb = True
    speed = [0, 0]
    sprite_bomb = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
    block_top = Utils.SIZE
    block_left = Utils.SIZE
    block_right = Utils.SIZE * 4
    block_bottom = Utils.SIZE * 4

    initialPos = None

    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        if not random.getrandbits(1):
            self.image, self.rect = load_png('images/small/player3.png')
        else:
            self.image, self.rect = load_png('images/small/player4.png')
        self.rect.topleft = pos
        self.idPlayer = id(self)
        self.initialPos = pos
        self.playerName = "Je n'ai pas de pseudo"

    def setName(self, name):
        self.playerName = name

    def setInitialPosition(self):
        self.rect.topleft = self.initialPos

    def up(self):
        if self.rect.top >= 0:
            self.speed[1] = -5 * self.speedMultiply
            self.speed[0] = 0

    def down(self):
        if not (self.rect.bottom >= Utils.SCREEN_HEIGHT):
            self.speed[1] = 5 * self.speedMultiply
            self.speed[0] = 0

    def left(self):
        if not (self.rect.left <= 0):
            self.speed[0] = -5 * self.speedMultiply
            self.speed[1] = 0

    def right(self):
        if not (self.rect.right >= Utils.SCREEN_WIDTH):
            self.speed[0] = 5 * self.speedMultiply
            self.speed[1] = 0

    def stop(self):
        self.speed = [0, 0]

    def bomb(self):
        if self.canBomb:
            bomb = Bomb(self.rect.center)
            bomb.player = self
            bomb.set_params_from_player()
            self.sprite_bomb.add(bomb)
            self.canBomb = False
            return bomb
        return None

    def update(self):
        if self.rect.top <= self.block_top:
            self.rect.top = self.block_top
            if self.speed[1] <= 0:
                self.speed[1] = 0
        if self.rect.bottom >= self.block_bottom:
            self.rect.bottom = self.block_bottom
            if self.speed[1] >= 0:
                self.speed[1] = 0
        if self.rect.left <= self.block_left:
            self.rect.left = self.block_left
            if self.speed[0] <= 0:
                self.speed[0] = 0
        if self.rect.right >= self.block_right:
            self.rect.right = self.block_right
            if self.speed[0] >= 0:
                self.speed[0] = 0
        self.rect = self.rect.move(self.speed)
        self.speed = [0, 0]
        position_can_bomb = True
        for bomb in self.sprite_bomb:
            if Utils.get_case_from_position(bomb.rect.center) == Utils.get_case_from_position(
                    self.rect.center):  # On vérifie qu'il ne pose pas 2 fois la bombe au meme endroit
                position_can_bomb = False

        if len(self.sprite_bomb) < self.number_bomb_max and position_can_bomb:
            self.canBomb = True

    def rAz(self):
        self.sprite_bomb.empty()
        self.speedMultiply = 1
        self.range = 2  # Range of the explosion
        self.timer_bomb = 120
        self.number_bomb_max = 1
        self.canBomb = True


class Player1Client(Player1, ConnectionListener):
    def __init__(self, pos, idPlayer):
        Player1.__init__(self, pos)
        self.idPlayer = idPlayer

    def set_position(self, position):
        self.rect.center = position

    def update(self):
        self.Pump()
