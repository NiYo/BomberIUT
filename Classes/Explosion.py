# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import pygame

from PodSixNet.Connection import ConnectionListener
from Utils.Utils import load_png


class Explosion(pygame.sprite.Sprite):
    """ class of """
    sprite_image_number_max = 8
    number_update_max = 40
    timer = 0
    idExplosion = None

    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = load_png('images/small/explosion/fireball_hit_0001.png')
        self.rect.topleft = pos
        self.idExplosion = id(self)

    def update(self):
        if (self.timer / (self.number_update_max / self.sprite_image_number_max)) + 1 > self.sprite_image_number_max:
            pygame.sprite.Sprite.kill(self)
        else:
            image_number = int(round((self.timer / (self.number_update_max / self.sprite_image_number_max) + 1), 0))
            image_number_old = int(
                round(((self.timer - 1) / (self.number_update_max / self.sprite_image_number_max) + 1), 0))
            if image_number != image_number_old or image_number_old == -1:
                self.image, lol = load_png('images/small/explosion/fireball_hit_000' + str(image_number) + '.png')
            self.timer += 1


class ExplosionClient(Explosion, ConnectionListener):
    def __init__(self, pos, idExplosion, timer):
        Explosion.__init__(self, pos)
        self.idExplosion = idExplosion

    def set_position(self, position):
        self.rect.center = position

    def update(self):
        self.Pump()
        if (self.timer / (self.number_update_max / self.sprite_image_number_max)) + 1 > self.sprite_image_number_max:
            pygame.sprite.Sprite.kill(self)
        else:
            image_number = int(round((self.timer / (self.number_update_max / self.sprite_image_number_max) + 1), 0))
            image_number_old = int(
                round(((self.timer - 1) / (self.number_update_max / self.sprite_image_number_max) + 1), 0))
            if image_number != image_number_old or image_number_old == -1:
                self.image, lol = load_png('images/small/explosion/fireball_hit_000' + str(image_number) + '.png')
            self.timer += 1
