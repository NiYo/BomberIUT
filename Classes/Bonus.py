# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------

import pygame

from PodSixNet.Connection import ConnectionListener
from Utils.Utils import load_png

POWER = "power"
NUMBER = "number"


class Bonus(pygame.sprite.Sprite):
    type = None
    idBonus = None

    def __init__(self, pos, type):
        pygame.sprite.Sprite.__init__(self)
        if type == POWER:
            self.image, self.rect = load_png('images/small/bonus_puissance.png')
            self.type = type
        if type == NUMBER:
            self.image, self.rect = load_png('images/small/bonus_nombre.png')
            self.type = type
        self.rect.topleft = pos
        self.idBonus = id(self)


class BonusClient(Bonus, ConnectionListener):
    def __init__(self, pos, idBonus, type):
        Bonus.__init__(self, pos, type)
        if type == POWER:
            self.image, self.rect = load_png('images/small/bonus_puissance.png')
            self.type = type
        if type == NUMBER:
            self.image, self.rect = load_png('images/small/bonus_nombre.png')
            self.type = type
        self.rect.topleft = pos
        self.idBonus = idBonus

    def set_position(self, position):
        self.rect.center = position

    def update(self):
        self.Pump()
