# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------


import pygame

from Utils.Utils import load_png
from Wall import Wall
from Wall import WallClient


class WallDestruct(Wall):
    """ class of  """

    life = 1

    def __init__(self, pos):
        Wall.__init__(self, pos)
        self.image, self.rect = load_png('images/small/mur2.png')
        self.rect.topleft = pos
        self.isDestructible = True

    def update(self):
        pass


class WallDestructClient(WallClient):
    """ class of  """

    life = 1

    def __init__(self, pos, id):
        WallClient.__init__(self, pos, id)
        self.image, self.rect = load_png('images/small/mur2.png')
        self.rect.topleft = pos
        self.isDestructible = True

    def update(self):
        if self.life == 0:
            pygame.sprite.Sprite.kill(self)
