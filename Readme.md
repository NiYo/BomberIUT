# Bomberman

# Regles:
Deux joueurs:

Une bombe à la fois qui explose avec un range de 1 (au début)

On se déplace  de la taille d'une image (dossier small)

* Deux type de caisses
    * Destructible
    * Non destructible

Les cartes sont stockées dans des fichiers binaires dans un tableau d'entier.

Les calculs se font sur serveur

Les collisions sont détectées par collide qui nous renvoie le tableau d'objets

Ajouter un bonus :
    Kamehameha qui fait péter ou active des bombes

# Credit

* Pygame
* PodSixNet
* PowStudio (Sprite explosion)
