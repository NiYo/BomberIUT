#!/usr/bin/env python
# -*- coding: utf-8 -*-

# --------------------------------------------------------------------------------------
# (C) Copyright 2016 Nicolas LUVISON and MALLEMANCHE Yoann.
#
# link : https://gitlab.com/NiYo/BomberIUT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# --------------------------------------------------------------------------------------

import random
import sys

from pygame import *
from pygame import mixer

from Classes.Bomb import BombClient
from Classes.Bonus import BonusClient
from Classes.Explosion import ExplosionClient
from Classes.Player1 import Player1Client
from Classes.Player2 import Player2Client
from Classes.Wall import WallClient
from Classes.WallDestruct import WallDestructClient
from PodSixNet.Connection import connection, ConnectionListener
from Utils.Utils import *

sprite_wall = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
sprite_wallDestruct = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
sprite_player = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
sprite_bomb = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
sprite_explosion = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
sprite_bonus = pygame.sprite.RenderClear()  # creation d'un groupe de sprite
soundtrack_play = None  # son en arriere plan
soundExplosion = None  # son de l'explosion
soundtracks = None  # liste de son sous forme de tuple(arriere_plan,explosion)
text = None  # le text affiché
font = None
bg = (10, 10, 10)  # couleur fond texte
fg = (200, 200, 200)  # couleur texte
playerName = sys.argv[1]


class Client(ConnectionListener):
    run = True
    idPlayer = None
    looser = None

    def __init__(self, host, port):
        self.Connect((host, port))

    def Loop(self):
        connection.Pump()
        self.Pump()

    def Network(self, data):
        # print('message de type %s recu' % data['action'])
        pass

    # Network event/message callbacks #
    def Network_connected(self, data):
        print('connecte au serveur !')
        # connection.Send({"action": "run", "data": "data"})

    def Network_rAz(self, data):
        global text, font
        text = font.render("Attente de joueurs", 1, fg, bg)
        soundtrack_play.stop()
        self.idPlayer = None
        self.looser = None
        sprite_wall.empty()  # creation d'un groupe de sprite
        sprite_wallDestruct.empty()  # creation d'un groupe de sprite
        sprite_bomb.empty()  # creation d'un groupe de sprite
        sprite_explosion.empty()  # creation d'un groupe de sprite
        sprite_bonus.empty()  # creation d'un groupe de sprite
        sprite_player.empty()

    def Network_error(self, data):
        print 'error:', data['error'][1]
        connection.Close()

    def Network_disconnected(self, data):
        print 'Server disconnected'
        sys.exit()

    def Network_carte(self, data):
        pass

    def Network_run(self, data):
        print 'start'
        self.run = True

    def Network_idPlayer(self, data):
        self.idPlayer = data["idPlayer"]

    def Network_playerName(self, data):
        global text
        text = font.render("DESTROY " + data["playerName"], 1, fg, bg)

    def Network_wall(self, data):
        global text, font
        global soundtrack_play, soundExplosion, soundtracks
        soundtrack_play, soundExplosion = soundtracks[random.randint(0, 1)]  # récupération des sons de la partie
        soundtrack_play.play(loops=1)  # lancement du son
        for info in data["indestructibleWall"]:
            sprite_wall.add(WallClient(info["pos"], info["id"]))
        for info in data["destructibleWall"]:
            sprite_wallDestruct.add(WallDestructClient(info["pos"], info["id"]))

    def Network_players(self, data):
        # players create only if new player added, else updated
        # Create
        if len(sprite_player) != len(data["player1"]):
            clear(sprite_player)
            for info in data["player1"]:
                if len(sprite_player) == 0:
                    sprite_player.add(Player1Client(info["pos"], info["id"]))
                else:
                    if isinstance(sprite_player.sprites()[0], Player1Client):
                        sprite_player.add(Player2Client(info["pos"], info["id"]))
                    else:
                        sprite_player.add(Player1Client(info["pos"], info["id"]))
        # Update
        else:
            for player in sprite_player:
                for info in data["player1"]:
                    if player.idPlayer == info["id"]:
                        player.rect.topleft = info["pos"]

    def Network_bombs(self, data):
        global soundExplosion
        # Bomb : update only if new/destroyed
        if len(sprite_bomb) != len(data["bomb"]):
            clear(sprite_bomb)
            for info in data["bomb"]:
                soundExplosion.play()
                sprite_bomb.add(BombClient(info["pos"], info["id"]))

    def Network_explosions(self, data):
        for info in data["explosion"]:
            sprite_explosion.add(ExplosionClient(info["pos"], info["id"], info["timer"]))

    def Network_looser(self, data):
        global text, font
        self.looser = data["looser"]
        if self.looser == self.idPlayer:
            text = font.render("Vous avez perdu", 1, fg, bg)
        else:
            text = font.render("Vous avez gagne", 1, fg, bg)

    def Network_walls(self, data):
        id_destructed = []
        for wall in data["wallDestruct"]:
            id_destructed.append(wall["id"])
        for wall in sprite_wallDestruct:
            if wall.idWall in id_destructed:
                pygame.sprite.Sprite.kill(wall)

    def Network_bonus(self, data):
        # Bonus : update only if new/destroyed
        if len(sprite_bonus) != len(data["bonus"]):
            clear(sprite_bonus)
            for info in data["bonus"]:
                sprite_bonus.add(BonusClient(info["pos"], info["id"], info["type"]))

    def Network_all(self, data):
        id_destructed = []
        for wall in data["wallDestruct"]:
            id_destructed.append(wall["id"])
        for wall in sprite_wallDestruct:
            if wall.idWall in id_destructed:
                pygame.sprite.Sprite.kill(wall)

        if len(sprite_bonus) != len(data["bonus"]):
            clear(sprite_bonus)
            for info in data["bonus"]:
                sprite_bonus.add(BonusClient(info["pos"], info["id"], info["type"]))

        for info in data["explosion"]:
            sprite_explosion.add(ExplosionClient(info["pos"], info["id"], info["timer"]))

        global soundExplosion
        # Bomb : update only if new/destroyed
        if len(sprite_bomb) != len(data["bomb"]):
            clear(sprite_bomb)
            for info in data["bomb"]:
                soundExplosion.play()
                sprite_bomb.add(BombClient(info["pos"], info["id"]))

        # players create only if new player added, else updated
        # Create
        if len(sprite_player) != len(data["player1"]):
            clear(sprite_player)
            for info in data["player1"]:
                if len(sprite_player) == 0:
                    sprite_player.add(Player1Client(info["pos"], info["id"]))
                else:
                    if isinstance(sprite_player.sprites()[0], Player1Client):
                        sprite_player.add(Player2Client(info["pos"], info["id"]))
                    else:
                        sprite_player.add(Player1Client(info["pos"], info["id"]))
        # Update
        else:
            for player in sprite_player:
                for info in data["player1"]:
                    if player.idPlayer == info["id"]:
                        player.rect.topleft = info["pos"]


def clear(sprite):
    sprite.empty()


def main_function():
    global soundtrack_play, soundExplosion, text, font, playerName, soundtracks
    """Main function of the game"""
    # Initialization
    pygame.init()

    # Text info
    font = pygame.font.Font("font/VCR_OSD_MONO_1.001.ttf", 36)
    text = font.render("Attente de joueurs", 1, fg, bg)
    textpos = text.get_rect()

    # sounds initialization
    soundtracks = [(mixer.Sound('sounds/partie_cool.ogg'), mixer.Sound('sounds/explosion_cool.ogg')),
                   (mixer.Sound('sounds/partie_btx.ogg'), mixer.Sound('sounds/explosion_btx.ogg'))]

    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    # ajout du texte
    textpos.centerx = screen.get_rect().centerx + SIZE / 2
    background_image, background_rect = load_png('images/small/dirt.png')
    clock = pygame.time.Clock()
    pygame.key.set_repeat(1, 1)
    pygame.display.set_caption("Client")
    client = Client(sys.argv[2], int(sys.argv[3]))
    # Objects creation

    connection.Send({"action": "playerName", "playerName": playerName})

    # MAIN LOOP
    while True:
        clock.tick(60)  # max speed is 60 frames per second
        connection.Pump()
        client.Pump()
        # Events handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return  # closing the window exits the program
        for x in range(SCREEN_WIDTH / SIZE):
            for y in range(SCREEN_HEIGHT / SIZE):
                screen.blit(background_image, (x * background_rect.h, y * background_rect.w))
        if client.run:
            touches = pygame.key.get_pressed()
            if client.looser == None:
                connection.Send({"action": "keys", "keystrokes": touches, "idPlayer": client.idPlayer})
            update()
            draw(screen)
            screen.blit(text, textpos)

        pygame.display.flip()


def draw(screen):
    sprite_wall.draw(screen)
    sprite_wallDestruct.draw(screen)
    sprite_bomb.draw(screen)
    sprite_bonus.draw(screen)
    sprite_player.draw(screen)
    sprite_explosion.draw(screen)


def update():
    sprite_wall.update()
    sprite_wallDestruct.update()
    sprite_bomb.update()
    sprite_bonus.update()
    sprite_player.update()
    sprite_explosion.update()


main_function()
sys.exit(0)
